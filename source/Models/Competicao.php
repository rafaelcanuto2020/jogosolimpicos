<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Competicao extends DataLayer{

    public function __construct()
    {
        parent:: __construct("competicoes",["nome","status"],"id",false);
    }
}