<?php
namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Resultados extends DataLayer{
    public function __construct()
    {
        parent:: __construct("resultados",["id_competicao","atleta","valor","unidade"],"id",false);
    }
}