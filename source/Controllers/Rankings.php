<?php
namespace Source\Controllers;
use Source\Models\Resultados;
require "../../vendor/autoload.php";
require "../config.php";
switch($_SERVER["REQUEST_METHOD"])
{
    case 'GET':
        header("HTTP/1.1 200 OK");
        $data = json_decode(file_get_contents("php://input"), false);
        if(!$data)
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Nenhum dado informado!"));
            exit;   
        }
        $rankings = new Resultados();
        if($ranking = $rankings->find("id_competicao = :name", "name={$data->id_competicao}"))
        {
            $return = array();
            foreach($rankings->find("id_competicao = :name", "name={$data->id_competicao}")->order('valor ASC')->fetch(true) as $ranking)
            {
                array_push($return,$ranking->data());
            }
            echo json_encode(array("response"=>$return));
        }
        else
        {
            echo json_encode(array("response"=>"Nenhum resultado cadastrado!"));
        }
    break;
}