<?php
namespace Source\Controllers;

use Source\Models\Competicao;
use Source\Models\Validations;

require "../../vendor/autoload.php";
require "../config.php";

switch($_SERVER["REQUEST_METHOD"]){
    case 'POST':

        $data = json_decode(file_get_contents("php://input"), false);
        if(!$data)
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Nenhum dado informado!"));
            exit;   
        }
        $erros = array();
        if(!Validations::validationsString($data->nome)){
            array_push($erros,"Nome:");
        }
        if(!Validations::validationsString($data->status)){
            array_push($erros,"Status:");
        }
        $competicoes = new Competicao();
        $competicoes->nome = $data->nome;
        $competicoes->status = $data->status;
        $competicoes->save();
        if($competicoes->fail()){
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode(array("response" => $competicoes->fail()->getMessage())); 
            exit;
        }

        header("HTTP/1.1 201 Created");
        echo json_encode(array("response"=> "Gravado com sucesso!!"));

    break;
    case "GET":
        header("HTTP/1.1 200 OK");
        $competicoes = new Competicao();
        if($competicoes->find()->Count()>0){
            $return = array();
            foreach($competicoes->find()->fetch(true) as $competicao){
                //tratamento de dados vindos do banco.
                array_push($return,$competicao->data());
            }
            echo json_encode(array("response"=>$return));
        }else{

            echo json_encode(array("response"=>"Nenhuma competição cadastrada!!"));
        }
    break;
    case 'PUT':
        $competicaoId = filter_input(INPUT_GET,"id");
        if(!$competicaoId){
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"id não informado"));
            exit;
        }
        $data = json_decode(file_get_contents('php://input'),false);
        if(!$data)
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Nenhum dado informado!"));
            exit;   
        }
        $erros = array();
        if(!Validations::validationsString($data->nome)){
            array_push($erros,"Nome:");
        }
        if(!Validations::validationsString($data->status)){
            array_push($erros,"Status");
        }
        if(count($erros)>0){

            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Há campos inválidos no formulário","fields"=>$erros));
            exit;
        }
        $competicaoI = (new Competicao())->findById($competicaoId);
        $competicaoI->nome = $data->nome;
        $competicaoI->status = $data->status;
        $competicaoI->save();
        if($competicaoI->fail()){
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode(array("response" => $competicoes->fail()->getMessage()));
            exit;
        } 
        header("HTTP/1.1 201 Created");
        echo json_encode(array("response"=>"Dados atualizados com sucesso!"));

    break;
    
    
    case 'DELETE':
        $competicaoId = filter_input(INPUT_GET,"id");
        if(!$competicaoId){
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"id não informado"));
        }
        $competicaoD = (new Competicao())->findById($competicaoId);
        $competicaoD->destroy();
        if($competicaoD->fail())
        {
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode(array("Responde"=>$competicaoD->fail()->getMessage()));
            exit;
        }
        header("HTTP/1.1 200 OK");
        echo json_encode(array("response"=>"Dado deletado com sucesso!"));
    break;
    default:
        header("HTTP/1.1 401 Unauthorized");
        echo json_encode(array("response"=>"Método não autorizado"));
    break;
}

