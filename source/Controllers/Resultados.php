<?php

namespace Source\Controllers;

use Source\Models\Competicao;
use Source\Models\Resultados;
use Source\Models\Validations;

require "../../vendor/autoload.php";
require "../config.php";

switch($_SERVER["REQUEST_METHOD"]){
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), false);
        if(!$data)
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Nenhum dado informado!"));
            exit;   
        }
        $competicaoS = new Competicao();
        $status = $competicaoS->findById($data->id_competicao);
        $erros = array();
        if(!Validations::validationsString($data->atleta)){
            array_push($erros,"Atleta:");
        }
        if(!Validations::validationInteger($data->valor)){
            array_push($erros,"Valor:");
        }
        if(!Validations::validationInteger($data->id_competicao)){
            array_push($erros,"ID da competição:");
        }
        if(!Validations::validationsString($data->unidade)){
            array_push($erros,"Unidade:");
        }
        $resultadosP = new Resultados();
        $resultadosP->atleta = $data->atleta;
        $resultadosP->valor = $data->valor;
        $resultadosP->unidade = $data->unidade;
        $resultadosP->id_competicao = $data->id_competicao;
        if($status->status == "F")
        {
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode(array("response"=>"Competição finalizada, não pode gravar os registros!"));
            exit;
        }
        else
        {
            $resultadosP->save();
        }
        if($resultadosP->fail()){
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode(array("response" => $resultadosP->fail()->getMessage())); 
            exit;
        }
        header("HTTP/1.1 201 Created");
        echo json_encode(array("response"=> "Gravado com sucesso!!"));
    break;
    case 'DELETE':
        $resultadoId = filter_input(INPUT_GET,"id");
        if(!$resultadoId){
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"id não informado"));
        }
        $resultadoD = (new Resultados())->findById($resultadoId);
        $resultadoD->destroy();
        if($resultadoD->fail())
        {
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode(array("Responde"=>$resultadoD->fail()->getMessage()));
            exit;
        }
        header("HTTP/1.1 200 OK");
        echo json_encode(array("response"=>"Dado deletado com sucesso!"));
    break;
    case "GET":
        header("HTTP/1.1 200 OK");
        $resultados = new Resultados();
        if($resultados->find()->Count()>0){
            $return = array();
            foreach($resultados->find()->fetch(true) as $resultado){
                //tratamento de dados vindos do banco.
                array_push($return,$resultado->data());
            }
            echo json_encode(array("response"=>$return));
        }else{
            echo json_encode(array("response"=>"Nenhuma competição cadastrada!!"));
        }
        
    break;

    case 'PUT':
        $resultadoId = filter_input(INPUT_GET,"id");
        if(!$resultadoId){
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"id não informado"));
            exit;
        }
        $data = json_decode(file_get_contents('php://input'),false);
        if(!$data)
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Nenhum dado informado!"));
            exit;   
        }
        $erros = array();
        if(!Validations::validationsString($data->atleta)){
            array_push($erros,"Atleta:");
        }
        if(!Validations::validationInteger($data->valor)){
            array_push($erros,"Valor:");
        }
        if(!Validations::validationInteger($data->id_competicao)){
            array_push($erros,"ID da competição:");
        }
        if(!Validations::validationsString($data->unidade)){
            array_push($erros,"Unidade:");
        }
        $resultadoI = (new Resultados())->findById($resultadoId);
        $resultadoI->atleta = $data->atleta;
        $resultadoI->valor = $data->valor;
        $resultadoI->id_competicao = $data->id_competicao;
        $resultadoI->unidade = $data->unidade;
        $resultadoI->save();
        if($resultadoI->fail()){
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode(array("response" => $resultados->fail()->getMessage()));
            exit;
        } 
        header("HTTP/1.1 201 Created");
        echo json_encode(array("response"=>"Dados atualizados com sucesso!"));

    break;
    default:
    header("HTTP/1.1 401 Unauthorized");
    echo json_encode(array("response"=>"Método não autorizado"));
break;
}